# ProjectAccount

[![CI Status](https://img.shields.io/travis/wang.wenyuan/ProjectAccount.svg?style=flat)](https://travis-ci.org/wang.wenyuan/ProjectAccount)
[![Version](https://img.shields.io/cocoapods/v/ProjectAccount.svg?style=flat)](https://cocoapods.org/pods/ProjectAccount)
[![License](https://img.shields.io/cocoapods/l/ProjectAccount.svg?style=flat)](https://cocoapods.org/pods/ProjectAccount)
[![Platform](https://img.shields.io/cocoapods/p/ProjectAccount.svg?style=flat)](https://cocoapods.org/pods/ProjectAccount)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ProjectAccount is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'ProjectAccount'
```

## Author

wang.wenyuan, wang.wenyuan@gloritysolutions.com

## License

ProjectAccount is available under the MIT license. See the LICENSE file for more info.
